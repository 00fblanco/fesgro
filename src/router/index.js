import Vue from 'vue';
import VueRouter from 'vue-router';
import requests from '../requests';

import Transparencia from '../components/Transparencia';
import Armonizacion from '../components/ArmonizacionContable'
import Dashboard from '../components/Dashboard';
import MisionVision from '../components/MisionVision';
import Organigrama from '../components/Organigrama'
import Directorio from '../components/Directorio'
import Home from '../components/Home';
import LogIn from '../components/LogIn';
import Objetivos from '../components/Objetivos';


const routes = [
    {path: '/', component: Home},
    { path: '/transparencia', component: Transparencia },
    { path: '/armonizacion_contable', component: Armonizacion },
    { 
      path: '/dashboard', component: Dashboard ,
      beforeEnter: validToken 
    },
    { path: '/mision_vision', component: MisionVision },
    { path: '/estructura', component: Organigrama },
    { path: '/directorio', component:Directorio},
    { path: '/inicio_sesion', component: LogIn},
    { path: '/objetivos', component: Objetivos }
  ]

const router = new VueRouter({
  routes // short for `routes: routes`
})

function validToken(to, from, next){
  if(window.Store.state.token == '')
    next('inicio_sesion');
  let formData = new FormData();
  formData.append('accion', 'valid_token');
  formData.append('token', window.Store.state.token);

  requests.validToken(formData).then(data => {
    if(data.data.status == "OK"){
      next();
    }else{
      next('/inicio_sesion');
    }
  })
}


  export default router;