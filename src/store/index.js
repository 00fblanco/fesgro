import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
const store = new Vuex.Store({
    state: {
        token: '',
        objetivos: 
            [ 
            "I.    Coordinar, formular y dirigir la política de Ciencia, Tecnología e innovación en el Estado.",
            "II. Dictar los lineamientos que orienten el desarrollo científico y tecnológico en la Entidad, tomando en cuenta los objetivos prioritarios y estratégicos previstos en el Programa Estatal de Ciencia, Tecnología e Innovación, en concordancia con el fin de contribuir a elevar los niveles de bienestar de la población y el desarrollo sustentable.",
            "III. Incentivar que un mayor número de investigadores, tecnólogos y académicos participen en labores de investigación científica y tecnológica."
            ]
        ,
        vision: 
            'Ser una institución reconocida a nivel estatal y nacional, al lograr que en el estado se desarrolle una cultura de apoyo a la ciencia y la innovación tecnológica, que contribuya al desarrollo de una economía sustentable para la población con una perspectiva de equidad de género: contando con la capacidad de enlace entre las instituciones generadoras de conocimiento científico, que contribuya preponderantemente a la emergencia y consolidación de programas de investigación y desarrollo tecnológico interinstitucional en beneficio de la población guerrerense.',
        mision:
            'Impulsar y fortalecer el desarrollo científico y la modernización tecnológica del Estado de Guerrero, mediante la formación de recursos humanos especializados de alto nivel, la promoción de proyectos específicos de investigación y la divulgación y difusión de la información científica y tecnológica.'
    },   
    mutations:{
        LOGIN(state, payload){
            state.token = payload.token;
        },
    },
    actions: {
        login(context, payload){
            context.commit('LOGIN', payload)
        }
    }  
})

export default store;