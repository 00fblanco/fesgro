import axios from 'axios';
axios.defaults.baseURL = 'http://cocytieg.gob.mx/api/';

export default {
    get_noticias(){
        let formData = new FormData();
        formData.append('accion', 'get_noticias');
        return axios.post('ControllerNoticia.php', 
            formData,
        ).then(data => {
            return data.data;
        }).catch(err => {
            return err;
        })
    },
    get_banner(){
        return axios.post('ControllerBanner.php')
        .then(data => {
            return data.data;
        }).catch(err => {
            return err;
        })
    },
    add_noticia(formData){
        return axios.post('ControllerNoticia.php',
            formData
        , {headers: {'Content-Type': 'multipart/form-data' }}
        ).then(response => {
            return response;
        }).catch(err => {
            return err;
        })
    },
    get_armonizacionCont(){
        let formData = new FormData();
        return axios.post('ControllerArmonizacion.php', 
            formData,
        ).then(data => {
            return data.data;
        }).catch(err => {
            return err;
        })
    },
    get_Transparencia(){
        let formData = new FormData();
        return axios.post('ControllerTransparencia.php', 
            formData,
        ).then(data => {
            return data.data;
        }).catch(err => {
            return err;
        })
    },
    logIn(formData){
        return axios.post('ControllerUsuario.php',
            formData
        ,
        ).then(response => {
            return response;
        }).catch(err => {
            return err;
        })
    },
    validToken(token){
        return axios.post('ControllerUsuario.php',
            token
        ,
        ).then(response => {
            return response;
        }).catch(err => {
            return err;
        })
    }

}