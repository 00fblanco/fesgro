import Vue from 'vue'
import App from './App.vue'

import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import VueRouter from 'vue-router'

Vue.use(VueRouter)
import store  from './store'
import router from './router';

import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'

import Vuex from 'vuex'

Vue.use(Vuex)
Vue.component('v-icon', Icon)

locale.use(lang)
Vue.use(ElementUI);
window.Store = store;
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
